<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Validation\Rule;

class UserController extends Controller
{

    // Show Register/Create Form
    public function create() {
        return view('users.register');
    }

    //login
    public function login() {
        return view('users.login');
    }

    //create new user
    public function store(Request $request) {

//        dd($request);
        $formFields = $request->validate([
            'name' => ['required', 'min:4'],
            'email' => ['required', 'email',Rule::unique('users', 'email')],
            'password' => 'required|confirmed|min:6'
        ]);

//        use bcrypt to hash password
        $formFields['password'] = bcrypt($formFields['password']);

//        dd($formFields);
        //create user
        $user =  User::create($formFields);
        // set user login on going
        auth()->login($user);

        return redirect('/')->with('message', 'User has created succesfully!!!');
    }

    // logout
    public function logout(Request $request) {
        auth()->logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect('/')->with('message', 'You have been logged out!');
    }

    // authenticate user
    public function authenticate(Request $request) {
        $formField = $request->validate([
            'email' => ['required','email'],
            'password' => 'required',
        ]);


        if(auth()->attempt($formField)) {
            $request->session()->regenerate();

            return redirect('/')->with('message', 'You are now logged in!');
        }
        return back()->withErrors(['email' => 'Invalid Credentials'])->onlyInput('email');
    }

}
