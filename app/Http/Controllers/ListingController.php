<?php

namespace App\Http\Controllers;

use App\Models\Listing;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ListingController extends Controller
{

    // Show all listings
    public function index() {
        return view('index',[
            'listings' => Listing::latest()->paginate(6)
        ]);
    }

    //show form
    public function show(Listing $listing) {
//        dd($listing);
        return view('show', [
            'listing' => $listing
        ]);
    }

    // show create form
    public function create() {
        return view('create');
    }
}
