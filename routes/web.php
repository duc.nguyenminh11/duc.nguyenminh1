<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ListingController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

// All Listings
Route::get('/', [ListingController::class, 'index']);

// Single Listing - new
Route::get('/listings/{listing}', [ListingController::class, 'show']);

//create form
Route::get('/listings/create',[ListingController::class,'store']);

// User login-out


// show register form
Route::get('/register', [UserController::class, 'create'])->middleware('guest');

// create new user
Route::post('/users', [UserController::class, 'store']);

//show login form
Route::get('/login',[UserController::class,'login'])->name('login');

//logout user

Route::post('/logout',[UserController::class,'logout'])->middleware('auth');

// Log In User
Route::post('/users/authenticate', [UserController::class, 'authenticate']);
